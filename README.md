## Feldspar

### Specifications

* [JSON Schema](http://json-schema.org/documentation.html)
* [JSON Patch](https://tools.ietf.org/html/rfc6902) RFC 6902
* [JSON Pointer](https://tools.ietf.org/html/rfc6901) RFC 6901

### Work Remaining

* [ ] Model
* [ ] Collection
* [ ] Schema
* [ ] Property
* [ ] Pointer
* [ ] Patch
* [ ] Mapping
